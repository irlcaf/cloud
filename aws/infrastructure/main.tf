terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = "4.31.0"
      configuration_aliases = [aws.us-east-1]
    }
  }
}

terraform {
  backend "s3" {
    bucket = var.bucket
    key    = var.key
    region = var.region
  }
}

provider "aws" {
  region = var.region
}

provider "aws" {
  alias  = "us-east-1"
  region = var.region_2
}

module "vpc" {
  source               = "./modules/vpc"
  vpc_name             = var.vpc_name
  environment          = var.environment
  s3_bucket_lb_logs_id = module.s3.s3_bucket_lb_logs_id
  cidr_block           = var.environment == "dev" ? "10.32.0.0/16" : "10.10.0.0/16"
  cidr_block_2         = "172.31.0.0/16"
  vpc_2                = try(module.vpc_2[0].vpc_id, "")
}

module "vpc_2" {
  count                              = var.environment == "prod" ? 1 : 0
  source                             = "./modules/vpc"
  vpc_name                           = var.vpc_name
  environment                        = "${var.environment}2"
  s3_bucket_lb_logs_id               = module.s3_2[0].s3_bucket_lb_logs_id
  cidr_block                         = "172.31.0.0/16"
  cidr_block_2                       = var.environment == "dev" ? "10.32.0.0/16" : "10.10.0.0/16"
  aws_vpc_peering_connection_peer_id = module.vpc.aws_vpc_peering_connection_peer_id
  providers = {
    aws = aws.us-east-1
  }
}

module "rds" {
  source               = "./modules/rds"
  private_subnets_1    = module.vpc.aws_private_subnets
  private_subnets_2    = try(module.vpc_2[0].aws_private_subnets, "")
  public_subnets       = module.vpc.aws_public_subnets
  environment          = var.environment
  db_master_username   = var.db_master_username
  db_master_password_1 = var.db_password_1
  db_master_password_2 = var.db_password_2
  vpc_1                = module.vpc.vpc_id
  vpc_2                = try(module.vpc_2[0].vpc_id, "")
  sg_rds_id_1          = module.vpc.sg_rds_id
  sg_rds_id_2          = try(module.vpc_2[0].sg_rds_id, "")
  providers = {
    aws.us-west-2 = aws
    aws.us-east-1 = aws.us-east-1
  }
}

module "ecs" {
  source                    = "./modules/ecs"
  cloudwatch_log_group_name = module.cloudwatch.cloudwatch_log_group_name
  environment               = var.environment
}

module "ecs_2" {
  count                     = var.environment == "prod" ? 1 : 0
  source                    = "./modules/ecs"
  cloudwatch_log_group_name = module.cloudwatch_2[0].cloudwatch_log_group_name
  environment               = "${var.environment}2"
  providers = {
    aws = aws.us-east-1
  }
}

module "cloudwatch" {
  source      = "./modules/cloudwatch"
  environment = var.environment
}

module "cloudwatch_2" {
  count       = var.environment == "prod" ? 1 : 0
  source      = "./modules/cloudwatch"
  environment = "${var.environment}2"
  providers = {
    aws = aws.us-east-1
  }
}

module "s3" {
  source      = "./modules/s3"
  environment = var.environment
}

module "s3_2" {
  count       = var.environment == "prod" ? 1 : 0
  source      = "./modules/s3"
  environment = "${var.environment}2"
  providers = {
    aws = aws.us-east-1
  }
}

module "ecr" {
  count       = var.environment == "prod" || var.environment == "dev" ? 0 : 1
  source      = "./modules/ecr"
  environment = var.environment
}

module "docdb" {
  source      = "./modules/documentdb"
  environment = var.environment
  password    = var.docdb_password
}
