variable "region" {
  type        = string
  description = "Name of the region deployed."
  default     = "us-west-2"
}

variable "region_2" {
  type        = string
  description = "Name of the region deployed."
  default     = "us-east-1"
}

variable "environment" {
  type        = string
  description = "Name of the enviornment the resources are beeing deployed for."
  default     = "dev"
}

variable "vpc_name" {
  type        = string
  description = "Name of the VPC to identify from others."
  default     = ""
}

variable "tag_policies" {
  type        = map(any)
  description = "Enforced tag policies for AWS Account"
  default = {
    Company            = "SpecialCompany"
    Owner              = "carlomagno@protonmail.com"
    DataClassification = "Sensitive"
    Product            = "SpecialProduct"
    Environment        = "Development"
  }
}

variable "docdb_password" {
  type        = string
  description = "Password for the docdb instance"
  sensitive   = true
  default     = ""
}

variable "db_password_1" {
  type        = string
  description = "Password for the db instance"
  sensitive   = true
  default     = ""
}

variable "db_master_username" {
  type        = string
  description = "Master username for the db instance."
  sensitive   = true
  default     = ""
}

variable "db_password_2" {
  type        = string
  description = "Password for the db instance"
  sensitive   = true
  default     = ""
}

variable "datadog_environment_vars" {
  type        = list(map(string))
  default     = [{}]
  description = "Datadog's environment vars."
}
