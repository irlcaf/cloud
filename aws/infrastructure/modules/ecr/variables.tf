variable "environment" {
  type        = string
  description = "Environment name."
  default     = "dev"
}

variable "ecr_repository_name" {
  type        = string
  description = "Name of the EC Repository for Services"
  default     = "backend-services"
}
