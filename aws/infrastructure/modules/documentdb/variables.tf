variable "environment" {
  type        = string
  description = "Environment name."
  default     = "dev"
}

variable "instance_name" {
  description = "Default instance name."
  default     = "backend-services"
}

variable "cluster_name" {
  description = "Default cluster name."
  default     = "backend-cluster"
}

variable "password" {
  description = "Docdb' instance password."
  sensitive   = true
}

//TODO Add terraform data to retrieve this programatically.
variable "availability_zones" {
  default     = ["us-east-2a", "us-east-2b", "us-east-2c"]
  description = "list of availability zones"
}

variable "tag_policies" {
  type        = map(any)
  description = "Enforced tag policies for AWS Account"
  default = {
    Company            = "SpecialCompany"
    Owner              = "carlomagno@protonmail.com"
    DataClassification = "Sensitive"
    Product            = "SpecialProduct"
    Environment        = "Development"
  }
}
