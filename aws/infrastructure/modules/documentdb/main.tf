resource "aws_docdb_cluster_instance" "cluster_instances" {
  identifier         = var.instance_name
  cluster_identifier = aws_docdb_cluster.docdb.id
  instance_class     = "db.t3.medium"
}

resource "aws_docdb_cluster" "docdb" {
  cluster_identifier  = var.cluster_name
  availability_zones  = var.availability_zones
  master_username     = "documentdb"
  master_password     = var.password
  storage_encrypted   = true
  deletion_protection = true
}
