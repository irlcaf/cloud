resource "aws_ec2_client_vpn_endpoint" "vpn" {
  description            = "Client VPN"
  client_cidr_block      = "10.31.0.0/16"
  split_tunnel           = true
  server_certificate_arn = "arn:aws:acm:us-west-2:065226150361:certificate/0d5299bc-61a0-487c-82fb-451e047e20ab"

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = "arn:aws:acm:us-west-2:065226150361:certificate/0d5299bc-61a0-487c-82fb-451e047e20ab"
  }
  connection_log_options {
    enabled = false
  }

}

resource "aws_ec2_client_vpn_network_association" "vpn_subnets" {
  count                  = length(var.aws_public_subnets)
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  subnet_id              = element(var.aws_public_subnets, count.index)
  security_groups        = [aws_security_group.vpn_access.id]
  lifecycle {
    ignore_changes = [subnet_id]
  }
}

resource "aws_ec2_client_vpn_authorization_rule" "vpn_auth_rule" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  target_network_cidr    = "0.0.0.0/0"
  authorize_all_groups   = true
}
