
variable "vpc_id" {
  description = "Id of the vpc used"
}

variable "aws_private_subnets" {
  description = "Id of the private subnets"
}

variable "aws_public_subnets" {
  description = "Id of the public subnets"
}

variable "environment" {
  type        = string
  description = "Name of the enviornment the resources are beeing deployed for."
  default     = "dev"
}
