variable "environment" {
  type        = string
  description = "Environment's name."
  default     = "dev"
}

variable "access_log_bucket_name" {
  type        = string
  description = "Access log bucket name."
  default     = "services-access-logs"
}
