resource "aws_db_instance" "postgresql_instance" {
  count                 = var.environment == "dev" || var.environment == "qa" ? 1 : 0
  allocated_storage     = 50
  identifier            = "${var.environment}-services"
  engine                = "postgres"
  engine_version        = "13.10"
  instance_class        = "db.t3.micro"
  username              = var.db_master_username
  password              = var.db_master_password_1
  parameter_group_name  = "default.postgres13"
  skip_final_snapshot   = true
  storage_encrypted     = true
  max_allocated_storage = 61
  publicly_accessible   = true
  db_subnet_group_name  = aws_db_subnet_group.postgres_sn_group[0].name
}

resource "aws_db_subnet_group" "postgres_sn_group" {
  count      = var.environment == "dev" || var.environment == "qa" ? 1 : 0
  name       = "${var.environment}_rds_groups"
  subnet_ids = var.public_subnets
}

resource "aws_rds_global_cluster" "global_cluster" {
  count                     = var.environment == "prod" ? 1 : 0
  global_cluster_identifier = "${var.environment}-services"
  force_destroy             = true
  engine                    = "aurora-postgresql"
  engine_version            = "15.2"
}

data "aws_availability_zones" "available_zones_us_west_2" {
  state    = "available"
  provider = aws.us-west-2
}

resource "aws_rds_cluster" "primary" {
  provider                  = aws.us-west-2
  count                     = var.environment == "prod" ? 1 : 0
  cluster_identifier        = "${var.environment}-primary"
  availability_zones        = slice(data.aws_availability_zones.available_zones_us_west_2.names, 0, 2)
  global_cluster_identifier = aws_rds_global_cluster.global_cluster[0].id
  engine                    = aws_rds_global_cluster.global_cluster[0].engine
  engine_version            = aws_rds_global_cluster.global_cluster[0].engine_version
  db_subnet_group_name      = aws_db_subnet_group.primary[0].name
  skip_final_snapshot       = true
  engine_mode               = "provisioned"
  master_username           = var.db_master_username
  master_password           = var.db_password_1
  vpc_security_group_ids    = [var.sg_rds_id_1]
  serverlessv2_scaling_configuration {
    max_capacity = 1.0
    min_capacity = 0.5
  }

  depends_on = [aws_rds_global_cluster.global_cluster[0]]
  lifecycle {
    ignore_changes = [cluster_identifier, availability_zones]
  }
}

resource "aws_rds_cluster_instance" "primary" {
  provider             = aws.us-west-2
  count                = var.environment == "prod" ? 1 : 0
  cluster_identifier   = aws_rds_cluster.primary[0].id
  engine               = aws_rds_global_cluster.global_cluster[0].engine
  engine_version       = aws_rds_global_cluster.global_cluster[0].engine_version
  identifier           = "${var.environment}-primary"
  instance_class       = "db.serverless"
  db_subnet_group_name = aws_db_subnet_group.primary[0].name
  depends_on           = [aws_rds_cluster.primary[0], aws_db_subnet_group.primary[0]]
}

resource "aws_db_subnet_group" "primary" {
  provider   = aws.us-west-2
  count      = var.environment == "prod" ? 1 : 0
  name       = "${var.environment}-primary"
  subnet_ids = var.private_subnets_1
  tags       = merge(var.tag_policies, { Name = "${var.environment} Primary Subnet Group" })
}

data "aws_availability_zones" "available_zones_us_east_1" {
  state    = "available"
  provider = aws.us-east-1
}

resource "aws_rds_cluster" "secondary" {
  provider                      = aws.us-east-1
  count                         = var.environment == "prod" ? 1 : 0
  cluster_identifier            = "${var.environment}-secondary"
  availability_zones            = slice(data.aws_availability_zones.available_zones_us_east_1.names, 0, 2)
  global_cluster_identifier     = aws_rds_global_cluster.global_cluster[0].id
  engine                        = aws_rds_global_cluster.global_cluster[0].engine
  engine_version                = aws_rds_global_cluster.global_cluster[0].engine_version
  db_subnet_group_name          = aws_db_subnet_group.secondary[0].name
  vpc_security_group_ids        = [var.sg_rds_id_2]
  replication_source_identifier = aws_rds_cluster.primary[0].arn
  engine_mode                   = "provisioned"
  skip_final_snapshot           = true

  serverlessv2_scaling_configuration {
    max_capacity = 1.0
    min_capacity = 0.5
  }
  depends_on = [aws_rds_global_cluster.global_cluster[0], aws_rds_cluster.primary[0]]
  lifecycle {
    ignore_changes = [cluster_identifier, availability_zones]
  }
}


resource "aws_rds_cluster_instance" "secondary" {
  provider             = aws.us-east-1
  count                = var.environment == "prod" ? 1 : 0
  cluster_identifier   = aws_rds_cluster.secondary[0].id
  engine               = aws_rds_global_cluster.global_cluster[0].engine
  engine_version       = aws_rds_global_cluster.global_cluster[0].engine_version
  identifier           = "${var.environment}-secondary"
  instance_class       = "db.serverless"
  db_subnet_group_name = aws_db_subnet_group.secondary[0].name

  depends_on = [aws_rds_cluster.secondary[0], aws_db_subnet_group.secondary[0]]

}

resource "aws_db_subnet_group" "secondary" {
  provider   = aws.us-east-1
  count      = var.environment == "prod" ? 1 : 0
  name       = "${var.environment}-secondary"
  subnet_ids = var.private_subnets_2

  tags = merge(var.tag_policies, { Name = "${var.environment} Secondary Subnet Group" })
}

resource "aws_route53_zone" "prod" {
  count = var.environment == "prod" ? 1 : 0
  name  = "prod_aurora_db_hosted_zones"
  vpc {
    vpc_id     = var.vpc_1
    vpc_region = "us-west-2"
  }
  vpc {
    vpc_id     = var.vpc_2
    vpc_region = "us-east-1"
  }
}
