variable "private_subnets_1" {
  description = "Private subnets created on the VPC module."
}

variable "private_subnets_2" {
  description = "Private subnets created on the VPC module."
}

variable "vpc_1" {
  description = "Private subnets created on the VPC module."
}

variable "vpc_2" {
  description = "Private subnets created on the VPC module."
}

variable "public_subnets" {
  description = "Private subnets created on the VPC module."
}

variable "environment" {
  description = "Environment's name."
  default     = "dev"
}

variable "tag_policies" {
  type        = map(any)
  description = "Enforced tag policies for AWS Account"
  default = {
    Company            = "SpecialCompany"
    Owner              = "carlomagno@protonmail.com"
    DataClassification = "Sensitive"
    Product            = "SpecialProduct"
    Environment        = "Development"
  }
}

variable "db_master_username" {
  type        = string
  description = "Master username for the db instance"
  sensitive   = true
}

variable "db_master_password_1" {
  type        = string
  description = "Password for the db instance"
  sensitive   = true
}

variable "db_master_password_2" {
  type        = string
  description = "Password for the db instance"
  sensitive   = true
}

variable "sg_rds_id_1" {
  type        = string
  description = "Id of the security group"
  default     = ""
}

variable "sg_rds_id_2" {
  type        = string
  description = "Id of the security group"
  default     = ""
}
