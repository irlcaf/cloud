
resource "aws_security_group" "sg_lb" {
  name        = "${var.environment} ${var.security_group_name}"
  description = "Allows TLS Inbound Traffic, HTTP and Ingress traffic for port 8080."
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "sg_http" {
  type              = "ingress"
  to_port           = 80
  protocol          = "tcp"
  from_port         = 80
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sg_lb.id
}

resource "aws_security_group_rule" "sg_https" {
  type              = "ingress"
  to_port           = 443
  protocol          = "tcp"
  from_port         = 443
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sg_lb.id
}

resource "aws_security_group_rule" "sg_custom" {
  type              = "ingress"
  to_port           = 8080
  protocol          = "tcp"
  from_port         = 8080
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sg_lb.id
}

resource "aws_security_group_rule" "sg_all" {
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  from_port         = 0
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.sg_lb.id
}

resource "aws_security_group" "sg_datadog" {
  name        = "${var.environment} datadog"
  description = "Allows TLS Inbound Traffic, HTTP and Ingress traffic for datadogs required prots."
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "sg_dd_udp" {
  type              = "ingress"
  to_port           = 8125
  protocol          = "udp"
  from_port         = 8125
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sg_datadog.id
}

resource "aws_security_group_rule" "sg_dd_all" {
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  from_port         = 0
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.sg_datadog.id
}

resource "aws_security_group" "sg_rds_cluster" {
  count       = var.environment == "prod" || var.environment == "prod2" ? 1 : 0
  name        = var.environment
  description = "Allows PSQL Inbound Traffic for port 5432."
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "sg_psql_ingress" {
  count             = var.environment == "prod" || var.environment == "prod2" ? 1 : 0
  type              = "ingress"
  to_port           = 5432
  protocol          = "tcp"
  from_port         = 5432
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sg_rds_cluster[0].id
}

resource "aws_security_group_rule" "sg_psql_egress" {
  count             = var.environment == "prod" || var.environment == "prod2" ? 1 : 0
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  from_port         = 0
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.sg_rds_cluster[0].id
}
