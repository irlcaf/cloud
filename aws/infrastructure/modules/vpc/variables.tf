variable "vpc_name" {
  type        = string
  description = "VPC name."
}

variable "tag_policies" {
  type        = map(any)
  description = "Enforced tag policies for AWS Account"
  default = {
    Company            = "SpecialCompany"
    Owner              = "carlomagno@protonmail.com"
    DataClassification = "Sensitive"
    Product            = "SpecialProduct"
    Environment        = "Development"
  }
}


variable "environment" {
  type        = string
  description = "Environment's name."
  default     = "dev"
}

variable "s3_bucket_lb_logs_id" {
  type        = string
  description = "Id for the s3 log bucket"
}

variable "security_group_name" {
  type        = string
  description = "Name of the security group attached to services"
  default     = "Services security group"
}

variable "load_balancer_name" {
  type        = string
  description = "Name of the loadbalancer provisioned for the KMS service."
  default     = "bs-lb"
}

variable "certificate_arn" {
  type        = string
  description = "ARN of the certificate attached to the load balancer."
  default     = ""
}

variable "cidr_block" {
  type        = string
  description = "CIDR Block for the VPC."
  default     = ""
}


variable "cidr_block_2" {
  type        = string
  description = "CIDR Block for the VPC #2."
  default     = ""
}

variable "vpc_2" {
  type        = string
  description = "VPC id for the peer connection"
  default     = ""
}

variable "aws_vpc_peering_connection_peer_id" {
  type        = string
  description = "AWS Peering Connection Id to be accepted.."
  default     = ""
}

variable "second_certificate_arn" {
  type        = string
  description = "Second ARN of the certificate attached to the load balancer."
  default     = ""
}
