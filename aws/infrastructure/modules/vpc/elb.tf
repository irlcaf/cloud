resource "aws_lb" "load_balancer" {
  name                       = "${var.environment}-${var.load_balancer_name}"
  subnets                    = toset(aws_subnet.public_1.*.id)
  security_groups            = [aws_security_group.sg_lb.id]
  load_balancer_type         = "application"
  enable_deletion_protection = true

  access_logs {
    bucket  = var.s3_bucket_lb_logs_id
    prefix  = "${var.environment}-bs-lb"
    enabled = true
  }

  depends_on = [
    aws_security_group.sg_lb
  ]
}

resource "aws_lb_listener" "lb_listener_http" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "lb_listener_https" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.certificate_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = ""
      status_code  = "200"
    }
  }
}

