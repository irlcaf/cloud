output "vpc_id" {
  description = "Name (id) of the vpc"
  value       = aws_vpc.vpc.id
}

output "aws_public_subnets" {
  value = aws_subnet.public_1.*.id
}

output "aws_private_subnets" {
  value = aws_subnet.private.*.id
}
output "vpc_details" {
  value = aws_vpc.vpc
}

output "psql_security_group" {
  value = aws_security_group.db_security_group.id
}

output "lb_listener_https_arn" {
  value = aws_lb_listener.lb_listener_https.arn
}

output "ecs_service_sg_id" {
  value = aws_security_group.sg_lb.id
}

output "load_balancer_arn" {
  value = aws_lb.load_balancer.arn
}

output "load_balancer_dns" {
  value = aws_lb.load_balancer.dns_name
}

output "sg_datadog" {
  value = aws_security_group.sg_datadog.id
}

output "aws_vpc_peering_connection_peer_id" {
  value = try(aws_vpc_peering_connection.prod[0].id, "")
}

output "sg_rds_id" {
  value = try(aws_security_group.sg_rds_cluster[0].id, "")
}
