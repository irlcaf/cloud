data "aws_availability_zones" "available_zones" {
  state = "available"
}

resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr_block
  enable_dns_hostnames = true
  tags                 = merge(var.tag_policies, { Name = "${var.environment} Infrastructure " })
  enable_dns_support   = true
}

resource "aws_subnet" "public_1" {
  count                   = 2
  cidr_block              = cidrsubnet(aws_vpc.vpc.cidr_block, 8, 2 + count.index)
  availability_zone       = data.aws_availability_zones.available_zones.names[count.index]
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = true
  tags                    = merge(var.tag_policies, { Name = "${var.environment} Public Subnet #${count.index + 1}" })
}

resource "aws_subnet" "private" {
  count             = 2
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 8, count.index)
  availability_zone = data.aws_availability_zones.available_zones.names[count.index]
  vpc_id            = aws_vpc.vpc.id
  tags              = merge(var.tag_policies, { Name = "${var.environment} Private Subnet #${count.index + 1}" })
}


resource "aws_vpc_peering_connection" "prod" {
  count       = var.environment == "prod" ? 1 : 0
  peer_vpc_id = var.vpc_2
  vpc_id      = aws_vpc.vpc.id
  peer_region = "us-east-1"
  tags = {
    Name = "VPC Peering for ${var.environment} environment"
  }
}

resource "aws_vpc_peering_connection_accepter" "peer" {
  count                     = var.environment == "prod2" ? 1 : 0
  vpc_peering_connection_id = var.aws_vpc_peering_connection_peer_id
  auto_accept               = true
  tags = {
    Side = "Accepter"
  }
}
