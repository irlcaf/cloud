resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.vpc.id
  tags   = merge(var.tag_policies, { Name = "${var.environment} Internet Gateway" })

}

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gateway.id
}

resource "aws_route" "peering_route" {
  count                     = var.environment == "prod" ? 1 : 0
  route_table_id            = aws_vpc.vpc.main_route_table_id
  destination_cidr_block    = var.cidr_block_2
  vpc_peering_connection_id = try(aws_vpc_peering_connection.prod[0].id, var.aws_vpc_peering_connection_peer_id)
}

resource "aws_route_table" "private" {
  count  = var.environment == "prod" ? 1 : 0
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block                = var.cidr_block_2
    vpc_peering_connection_id = try(aws_vpc_peering_connection.prod[0].id, var.aws_vpc_peering_connection_peer_id)
  }
  tags = merge(var.tag_policies, { Name = "${var.environment} Private Route Table #1" })
}

resource "aws_route_table_association" "public" {
  count          = length(aws_subnet.public_1.*.id)
  subnet_id      = element(aws_subnet.public_1.*.id, count.index)
  route_table_id = aws_vpc.vpc.main_route_table_id
}

resource "aws_route_table_association" "private" {
  count          = var.environment == "prod" ? length(aws_subnet.private.*.id) : 0
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private[0].id
}
