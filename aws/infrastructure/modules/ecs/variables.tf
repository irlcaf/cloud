variable "cloudwatch_log_group_name" {
  type        = string
  description = "Name of the cloudwatch log group name for blockchain services"
  default     = "serviceName1"
}

variable "ecs_cluster_name" {
  type        = string
  description = "Name of the ECS Cluster."
  default     = "services"
}

variable "environment" {
  type        = string
  description = "Environment name."
  default     = "dev"
}

variable "enable_container_insights" {
  type        = string
  description = "Boolean to enable container insights configurations."
  default     = "disabled"
}
