resource "aws_kms_key" "aws_kms" {
  description             = "KMS Service for ECS Cluster configuration"
  deletion_window_in_days = 7
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.environment}-${var.ecs_cluster_name}"

  setting {
    name  = "containerInsights"
    value = var.enable_container_insights
  }
  configuration {
    execute_command_configuration {
      kms_key_id = aws_kms_key.aws_kms.arn
      logging    = "OVERRIDE"

      log_configuration {
        cloud_watch_encryption_enabled = true
        cloud_watch_log_group_name     = var.cloudwatch_log_group_name
      }
    }
  }
}
