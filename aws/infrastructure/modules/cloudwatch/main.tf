resource "aws_cloudwatch_log_group" "cloudwatch_log" {
  name = "/${var.environment}/${var.cloudwatch_log_name}"
}

output "cloudwatch_log_group_name" {
  value = aws_cloudwatch_log_group.cloudwatch_log.name
}

variable "cloudwatch_log_name" {
  type        = string
  description = "Cloudwatch log group name"
  default     = "backend/services/"
}

variable "environment" {
  type        = string
  description = "Environment name."
  default     = "dev"
}
