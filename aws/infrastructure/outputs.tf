
output "regions" {
  value = [var.region, var.region_2]
}

output "vpc_id" {
  value = [module.vpc.vpc_id, try(module.vpc_2[0].vpc_id, null)]
}

output "aws_public_subnets" {
  value = [module.vpc.aws_public_subnets, try(module.vpc_2[0].aws_public_subnets, null)]
}
output "aws_private_subnets" {
  value = [module.vpc.aws_private_subnets, try(module.vpc_2[0].aws_private_subnets, null)]
}

output "cloudwatch_log_group_name" {
  value = [module.cloudwatch.cloudwatch_log_group_name, try(module.cloudwatch_2[0].cloudwatch_log_group_name, null)]
}

output "lb_dns_name" {
  value = [module.vpc.load_balancer_dns, try(module.vpc_2[0].load_balancer_dns, null)]
}

output "lb_listener_https_arn" {
  value = [module.vpc.lb_listener_https_arn, try(module.vpc_2[0].lb_listener_https_arn, null)]
}

output "ecs_service_sg" {
  value = [module.vpc.ecs_service_sg_id, try(module.vpc_2[0].ecs_service_sg_id, null)]
}

output "ecs_cluster_id" {
  value = [module.ecs.ecs_cluster_id, try(module.ecs_2[0].ecs_cluster_id, null)]
}

output "load_balancer_arn" {
  value = [module.vpc.load_balancer_arn, try(module.vpc_2[0].load_balancer_arn, null)]
}
