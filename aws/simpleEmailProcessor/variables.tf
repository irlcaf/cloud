variable "email_domain" {
  description = "Domain identify."
}

variable "tag_policies" {
  type        = map(any)
  description = "Enforced tag policies for AWS Account"
  default = {
    Company            = "SpecialCompany"
    Owner              = "carlomagno@protonmail.com"
    DataClassification = "Sensitive"
    Product            = "SpecialProduct"
    Environment        = "Development"
  }
}

variable "environment" {
  description = "Environment's name."
  default     = ""
}

variable "bucket" {
  description = "Email storage bucket."
  default     = ""
}

variable "lambda_environment_vars" {
  description = "Lambda function environment variables."
  sensitive   = true
  default     = {}
}

variable "region" {
  description = "Default environment variable for resources."
  default     = ""
}
