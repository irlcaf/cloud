terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.31.0"
    }
  }
}

terraform {
  backend "s3" {
    bucket = var.bucket
    key    = var.key
    region = var.region
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = var.tag_policies
  }
}

module "ses" {
  source        = "./ses"
  email_domain  = var.email_domain
  bucket        = module.s3.bucket
  environment   = var.environment
  sns_topic_arn = module.sns.sns_topic_arn
}

module "s3" {
  environment = var.environment
  source      = "./s3"
}

module "sns" {
  environment = var.environment
  lambda_arn  = module.lambda.lambda_arn
  source      = "./sns"
}

module "sqs" {
  environment = var.environment
  source      = "./sqs"
}

module "lambda" {
  source                  = "./lambda"
  email_queue_url         = module.sqs.sqs_queue_url
  environment             = var.environment
  s3_bucket               = module.s3.bucket
  s3_bucket_arn           = module.s3.bucket_arn
  topic_arn               = module.sns.sns_topic_arn
  email_domain            = var.email_domain
  email_queue_arn         = module.sqs.sqs_queue_arn
  lambda_environment_vars = var.lambda_environment_vars
  lambda_bucket           = module.s3.bucket_lambda
}
