variable "email_domain" {
  description = "Domain identify (dependant on the environment)"
  default     = "example.com"
}

variable "environment" {
  description = "Environment's name."
  default     = "prod"
}

variable "bucket" {
  description = "Environment variable used for naming conventions."
  default     = ""
}

variable "sns_topic_arn" {
  description = "SNS topic arn to send notifications email has been stored.."
  default     = ""
}
