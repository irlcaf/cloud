resource "aws_ses_domain_identity" "domain" {
  domain = var.email_domain
}

resource "aws_ses_receipt_rule" "store" {
  name          = "store"
  rule_set_name = aws_ses_receipt_rule_set.email.rule_set_name
  recipients    = ["${var.email_domain}"]
  enabled       = true
  scan_enabled  = true
  tls_policy    = "Require"

  s3_action {
    bucket_name = var.bucket
    position    = 1
    topic_arn   = var.sns_topic_arn
  }

}

resource "aws_ses_receipt_rule_set" "email" {
  rule_set_name = "email-rules"
}

data "aws_kms_key" "ses" {
  key_id = "alias/aws/ses"
}
