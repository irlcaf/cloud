variable "environment" {
  description = "Environment variable used for naming conventions."
  default     = ""
}

variable "account-id" {
  description = "Account ID"
  default     = "111111111"
}

variable "lambda_arn" {
  description = "Lambda ARN for subscription"
  default     = ""
}
