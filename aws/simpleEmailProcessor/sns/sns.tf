resource "aws_sns_topic" "email_received" {
  name            = "${var.environment}-emails"
  delivery_policy = <<EOF
  {
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false,
    "defaultRequestPolicy": {
      "headerContentType": "text/plain; charset=UTF-8"
    }
  }
}
  EOF
}

resource "aws_sns_topic_policy" "default" {
  arn = aws_sns_topic.email_received.arn

  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
    actions = [
      "SNS:Publish",
    ]
    condition {
      test     = "StringEquals"
      variable = "AWS:SourceAccount"

      values = [
        var.account-id,
      ]
    }

    condition {
      test     = "StringLike"
      variable = "AWS:SourceArn"
      values = [
        "arn:aws:ses:*",
      ]
    }
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ses.amazonaws.com"]
    }

    resources = [
      aws_sns_topic.email_received.arn,
    ]
    sid = "AllowSESPublish"
  }
}

resource "aws_sns_topic_subscription" "lambda_subscription" {
  topic_arn = aws_sns_topic.email_received.arn
  protocol  = "lambda"
  endpoint  = var.lambda_arn
}

output "sns_topic_arn" {
  value = aws_sns_topic.email_received.arn
}
