resource "aws_lambda_function" "archiver_daemon" {
  filename         = aws_s3_object.lambda_object.id
  function_name    = "${var.environment}-${var.lambda_function_name}"
  role             = aws_iam_role.lambda_role.arn
  handler          = var.handler_function # This needs to be set correctly.
  runtime          = "nodejs16.x"
  memory_size      = 256
  timeout          = 600
  architectures    = ["x86_64"]
  source_code_hash = aws_s3_object.lambda_object.source_hash
  environment {
    variables = local.environment_vars
  }

  depends_on = [
    aws_cloudwatch_log_group.lambda_logs,
  ]
}

resource "aws_s3_object" "lambda_object" {
  bucket = var.lambda_bucket
  key    = "key"
  source = "${path.cwd}/source.zip"
}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = path.cwd
  output_path = "${path.cwd}/source.zip"
}

resource "aws_lambda_function" "sqs_archiver_daemon" {
  filename         = aws_s3_object.lambda_object.id
  function_name    = "${var.environment}-${var.lambda_function_name_2}"
  role             = aws_iam_role.lambda_role.arn
  handler          = var.handler_function_sqs # This needs to be set correctly.
  runtime          = "nodejs16.x"
  memory_size      = 256
  timeout          = 600
  architectures    = ["x86_64"]
  source_code_hash = aws_s3_object.lambda_object.source_hash
  environment {
    variables = local.environment_vars
  }

  depends_on = [
    aws_cloudwatch_log_group.lambda_logs_2,
  ]
}

resource "aws_iam_role" "lambda_role" {
  name = "${var.environment}lambda"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy" "lambda_policy" {
  role = aws_iam_role.lambda_role.name
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "s3:ListBucket",
        Resource = ["${var.s3_bucket_arn}"]
      },
      {
        Effect   = "Allow",
        Action   = ["s3:GetObject", "s3:DeleteObject"],
        Resource = ["${var.s3_bucket_arn}/*"]
      },
      {
        Effect   = "Allow",
        Action   = ["sqs:ReceiveMessage", "sqs:DeleteMessage", "sqs:GetQueueAttributes", "sqs:SendMessage"],
        Resource = ["${var.email_queue_arn}"]
      }
    ]
  })
}

output "lambda_arn" {
  value = aws_lambda_function.archiver_daemon.arn
}

resource "aws_lambda_permission" "allow_sns" {
  statement_id  = "AllowSNSToInvokeFunction"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.archiver_daemon.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = var.topic_arn
}

resource "aws_lambda_event_source_mapping" "event_source_mapping" {
  event_source_arn = var.email_queue_arn
  enabled          = true
  function_name    = aws_lambda_function.sqs_archiver_daemon.arn
}

resource "aws_cloudwatch_log_group" "lambda_logs" {
  name              = "/aws/lambda/${var.environment}-${var.lambda_function_name}"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "lambda_logs_2" {
  name              = "/aws/lambda/${var.environment}-${var.lambda_function_name_2}"
  retention_in_days = 14
}

data "aws_iam_policy_document" "lambda_logging" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:*"]
  }
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "${var.environment}emailLogs"
  path        = "/"
  description = "IAM policy for logging from a lambda"
  policy      = data.aws_iam_policy_document.lambda_logging.json
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

locals {
  environment_vars = merge(var.lambda_environment_vars, { ALLOWED_DOMAIN_EMAIL_DOMAIN = "@${var.email_domain}" }, { S3_BUCKET = var.s3_bucket }, { SQS_QUEUE_URL = var.email_queue_url })
}
