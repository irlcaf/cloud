variable "s3_bucket" {
  description = "Email storage bucket."
  default     = ""
}

variable "s3_bucket_arn" {
  description = "Email storage bucket arn."
  default     = ""
}

variable "topic_arn" {
  description = "Subscription policy topic arn."
}

variable "email_domain" {
  description = "Domain identify."
  default     = "example.com"
}

variable "environment" {
  description = "Environment variable used for naming conventions."
  default     = "dev"
}

variable "email_queue_arn" {
  description = "SQS queue arn."
  default     = ""
}

variable "email_queue_url" {
  description = "SQS queue URL."
  default     = ""
}

variable "lambda_function_name" {
  default = "lambda_name"
}

variable "lambda_function_name_2" {
  default = "lambda_name_2"
}

variable "handler_function" {
  default = "src/index.main"
}

variable "handler_function_sqs" {
  default = "src/index.sqs"
}

variable "lambda_environment_vars" {
  sensitive = true
  default   = {}
}

variable "lambda_bucket" {
  description = "Lambda storage bucket."
  default     = ""
}
