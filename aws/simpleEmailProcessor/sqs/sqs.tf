resource "aws_sqs_queue" "queue" {
  name                       = "${var.environment}-emails"
  delay_seconds              = 90
  max_message_size           = 2048
  message_retention_seconds  = 86400
  receive_wait_time_seconds  = 10
  visibility_timeout_seconds = 600
}

output "sqs_queue_arn" {
  value = aws_sqs_queue.queue.arn
}

output "sqs_queue_url" {
  value = aws_sqs_queue.queue.url
}

variable "environment" {
  description = "Environment's name."
  default     = "dev"
}
