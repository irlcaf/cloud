resource "aws_s3_bucket" "email_bucket" {
  bucket = "${var.environment}-emails"
}

resource "aws_s3_bucket" "lambda_bucket" {
  bucket = "${var.environment}-emails-lambda"
}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    principals {
      type        = "Service"
      identifiers = ["ses.amazonaws.com"]
    }

    actions = [
      "s3:PutObject",
    ]

    resources = [
      aws_s3_bucket.email_bucket.arn,
      "${aws_s3_bucket.email_bucket.arn}/*",
    ]

    condition {
      test     = "StringEquals"
      values   = ["065226150361"]
      variable = "AWS:SourceAccount"
    }
    condition {
      test     = "StringLike"
      values   = ["arn:aws:ses:*"]
      variable = "AWS:SourceArn"
    }
  }
  statement {
    principals {
      type        = "AWS"
      identifiers = ["065226150361"]
    }

    actions = [
      "s3:GetObject",
    ]

    resources = [
      aws_s3_bucket.email_bucket.arn,
      "${aws_s3_bucket.email_bucket.arn}/*",
    ]

    condition {
      test     = "Bool"
      values   = [false]
      variable = "aws:SecureTransport"
    }
  }
}

resource "aws_s3_bucket_policy" "email_bucket_policy" {
  bucket = aws_s3_bucket.email_bucket.bucket
  policy = data.aws_iam_policy_document.bucket_policy.json
}

data "aws_kms_key" "s3_kms_key" {
  key_id = "alias/aws/s3"
}

output "bucket" {
  value = aws_s3_bucket.email_bucket.bucket
}

output "bucket_arn" {
  value = aws_s3_bucket.email_bucket.arn
}

output "bucket_lambda" {
  value = aws_s3_bucket.lambda_bucket.bucket
}
