# AWS Services Templates

Welcome to the Terraform AWS Services Templates repository! Here you will find a collection of Terraform templates to provision various AWS services easily and efficiently.

## Table of Contents

1. [Introduction](#introduction)
2. [Usage](#usage)
3. [Directory Structure](#directory-structure)
4. [Contributing](#contributing)
5. [License](#license)

## Introduction

This repository contains Terraform templates to deploy different AWS services such as EC2 instances, S3 buckets, RDS databases, Lambda functions, and more. These templates are designed to simplify the process of provisioning AWS resources by using infrastructure as code principles.

Using Terraform, you can define your infrastructure requirements in code, making it easier to manage, version control, and reproduce your infrastructure setups across different environments.

## Usage

To use these Terraform templates, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the directory of the service you want to deploy.
3. Customize the `terraform.tfvars` file with your AWS credentials and any other configuration options specific to your setup.
4. Initialize the Terraform configuration by running `terraform init`.
5. Review the execution plan by running `terraform plan`.
6. Apply the changes by running `terraform apply`.

## Directory Structure

AWS folder contains some sample terraform infrastructure code for the creation of a backend service application.
.
├── aws
├──── infrastructure # AWS infrastructure project that includes vpc network design and attachment of different services dependent distributed accross the cloud networks.
├─────── main.tf # Terraform main file configuration, module definitions, providers and state configuration
├─────── modules # Terraform modules for aws services
├────────── cloudwatch
├────────── documentdb
├────────── ecr (Elastic Container Registry)
├────────── rds (Relational Database Service)
├────────── s3 (Simple storage service)
├────────── vpc (AWS networks)
├────────── vpn
├───── simpleEmailProcessor
├─────── main.tf # Main file configuration, module definitions, providers and state configuration.
├────────── lambda
├────────── s3
├────────── ses
├────────── sns
├────────── sqs

Projects folder include a very high level architecture diagram presented to the client as proposal, based on discussed requirements with the client.s
.
├── projects

## Contributing

Contributions to this repository are welcome! If you have any improvements, additional Terraform templates, or fixes, feel free to open a pull request. Please ensure your code follows the established conventions and includes relevant documentation.

## License

This repository is licensed under the [MIT License](LICENSE). Feel free to use the provided Terraform templates for your own projects.
